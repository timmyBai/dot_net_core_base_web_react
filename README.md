# dot net core base web react

<div>
    <img src="https://img.shields.io/badge/react-18.2.0-blue">
    <img src="https://img.shields.io/badge/react dom-18.2.0-blue">
    <img src="https://img.shields.io/badge/react router dom-6.11.0-blue">
    <img src="https://img.shields.io/badge/@reduxjs/toolkit-1.9.7-blue">
</div>

## 簡介

dot net core base web react 是一個撰寫共用邏輯寫法所建立的架構，它基於 react 實建，它使用最新前端技術，提供完整的架構建置，可以幫助您快速建立企業級的架構，相信不管遇到什麼需求，這個架構可以幫助您，建置完美架構。

## 功能

```tex
- 首頁
- websocket
- pdf report
- excel report
```

## 目標後端專案

<div>
    <a href=""></a>
</div>

<div>
   <a href="https://gitlab.com/timmyBai/dot_net_core_base_web_api" target="_blank">後端專案地址</a>
</div>

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
