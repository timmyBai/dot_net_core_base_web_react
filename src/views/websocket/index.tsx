import React, { useEffect, useState } from 'react';

// css
import './index.sass';

const WebSocketHandle: React.FC = () => {
    const [websocket, setWebsocket] = useState<WebSocket | null>(null);
    const [messages, setMessages] = useState<string>('');

    const createWebSocket = () => {
        const ws = new WebSocket('ws://localhost:5000/ws');

        ws.onopen = () => {
            console.log('connected');
        };

        ws.onmessage = (event) => {
            const data = event.data;

            setMessages(state => state += data + '\n');
        };

        ws.onclose = () => {
            setWebsocket(null);
        };

        setWebsocket(ws);
    };

    const sendWebsocketMessage = (message: string) => {
        if (websocket && websocket.readyState === WebSocket.OPEN) {
            websocket.send(message);
        }
        else {
            console.error('WebSocket not open');
        }
    };

    useEffect(() => {
        createWebSocket();

        return () => {
            if (websocket !== null) {
                websocket.close();
            }
        };
    }, []);

    return (
        <div className='websocket'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <button className='btn btnSendApple' onClick={() => sendWebsocketMessage('送出蘋果')}>送出蘋果</button>
                        <button className='btn btnSendStrawberry' onClick={() => sendWebsocketMessage('送出草莓')}>送出草莓</button>
                        <button className='btn btnSendCockroach' onClick={() => sendWebsocketMessage('送出蟑螂')}>送出蟑螂</button>
                        
                        <p>回傳訊息</p>

                        <textarea name='messagebox' defaultValue={messages} readOnly={true}></textarea>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default WebSocketHandle;