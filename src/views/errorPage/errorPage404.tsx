import React from 'react';

// css
import './errorPage404.sass';

const ErrorPage404: React.FC = () => {
    return (
        <div className='errorPage404'>
            404
        </div>
    );
};

export default ErrorPage404;