import React from 'react';

// css
import './index.sass';

// api
import { excelReport } from '@/api/execlReport';

// utils
import { axiosErrorCode } from '@/utils/request';

const ExcelReport: React.FC = () => {
    const downloadExcelReport = () => {
        excelReport().then((res) => {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.style.display = 'none';
            link.href = url;

            link.download = '學生就讀清單.xls';
            document.body.appendChild(link);
            link.click();
            link.remove();
        }).catch((error) => {
            const errorCode = axiosErrorCode<ExcelReportErrorCodeType>(error);

            switch(errorCode) {
                default:
                    console.log('伺服器錯誤');
            }
        });
    };

    return (
        <div className='excelReport'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <button onClick={downloadExcelReport}>下載學生報表</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ExcelReport;