import React from 'react';

// css
import './index.sass';

const Home: React.FC = () => {
    return (
        <div className='home'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            This is home page
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;