import React from 'react';

// css
import './index.sass';

// api
import { pdfReport } from '@/api/pdfReport';

// utils
import { axiosErrorCode } from '@/utils/request';

const PdfReport: React.FC = () => {
    const downloadPdfReport = () => {
        pdfReport().then((res) => {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.style.display = 'none';
            link.href = url;

            link.download = '學生清單.pdf';
            document.body.appendChild(link);
            link.click();
            link.remove();
        }).catch((error) => {
            const errorCode = axiosErrorCode<PdfReportErrorCodeType>(error);

            switch(errorCode) {
                default:
                    console.log('伺服器錯誤');
            }
        });
    };

    return (
        <div className='pdfReport'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <button onClick={downloadPdfReport}>學生清單報表下載</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PdfReport;