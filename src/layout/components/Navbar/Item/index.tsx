import React from 'react';

// css
import './index.sass';

type ItemPropsType = {
    title: string
}

const Item: React.FC<ItemPropsType> = ({ title }) => {
    return (
        <div className='item'>
            {title}
        </div>
    );
};

export default Item;
