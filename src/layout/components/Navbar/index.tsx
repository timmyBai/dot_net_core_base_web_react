// react
import React from 'react';

// css
import './index.sass';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// components
import Hamburger from '@/components/Hamburger/index';
import Menu from '@/components/Menu/Menu';
import Logo from './Logo/index';
import NavbarItem from './NavbarItem/index';

const Navbar: React.FC = () => {
    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state) => {
        return {
            sidebar: state.app.sidebar.opened ? 'close' : 'opened',
            menu: state.app.sidebar.opened ? 'opened' : 'close',
            device: state.app.device,
            routes: state.permission.routes
        };
    });

    const handleCollapse = () => {
        dispatch({ type: 'app/toggleSideBar' });
    };
    
    return (
        <div className='navbar'>
            <div className='container'>
                <div className='row'>
                    <div className='d_float_left'>
                        <Logo></Logo>
                    </div>
                    
                    <div className='d_float_right'>
                        <Hamburger className={`${storeGetters.sidebar} ${storeGetters.device}`} collapse={handleCollapse}></Hamburger>
                        <Menu className={`${storeGetters.menu} ${storeGetters.device}`}>
                            {
                                storeGetters.routes.map((route: any) => {
                                    return (
                                        <NavbarItem item={route} basePath={route.path} isNest={false} key={route.path}></NavbarItem>
                                    );
                                })
                            }
                        </Menu>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Navbar;
