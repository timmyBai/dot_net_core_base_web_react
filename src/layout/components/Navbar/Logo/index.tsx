import React from 'react';
import { NavLink } from 'react-router-dom';

// css
import './index.sass';

const Logo: React.FC = () => {
    return (
        <div className='logo'>
            <NavLink className='link' to='/'>dot net core base web frond</NavLink>
        </div>
    );
};

export default Logo;
