import React from 'react';

// utils
import { isExternal } from '@/utils/validate';

// css
import './index.sass';

// components
import MenuItem from '@/components/Menu/MenuItem';
import SubMenu from '@/components/Menu/SubMenu';
import AppLink from '../AppLink/index';
// import Item from '../Item/index.jsx';

let onlyOneChild: any = null;


type NavbarItemProps = {
    basePath?: string;
    item?: any;
    isNest: boolean;
}

const hasOneShowingChild = (children: Array<any> = [], parent: any) => {
    const showingChildren = children.filter((item: any) => {
        if (item.hidden) {
            return false;
        }
        else {
            onlyOneChild = item;
            return true;
        }
    });

    if (showingChildren?.length === 1) {
        return true;
    }

    if (showingChildren?.length === 0) {
        onlyOneChild = { ...parent, path: '', noShowingChildren: true };
        return true;
    }

    return false;
};

const resolvePath = (routePath: any, basePath: any) => {
    if (isExternal(routePath)) {
        return routePath;
    }

    if (isExternal(basePath)) {
        return basePath;
    }

    if (routePath.indexOf('/') !== -1) {
        return basePath + routePath;
    }
    else {
        return basePath + '/' + routePath;
    }
};

const NavbarItem: React.FC<NavbarItemProps> = ({ basePath, item, isNest }) => {
    if (!item.hidden) {
        if (hasOneShowingChild(item.children, item) && (!onlyOneChild.children || onlyOneChild?.noShowingChildren) && !item.alwaysShow) {
            return (
                <MenuItem>
                    <AppLink to={resolvePath(onlyOneChild.path, basePath)} title={onlyOneChild.meta.title} isNest={isNest}></AppLink>
                </MenuItem>
            );
        }

        if (item.children?.length) {
            return (
                <SubMenu title={item.meta.title}>
                    {
                        item.children?.map((child: any) => {
                            return (
                                <NavbarItem key={child.path} basePath={resolvePath(child.path, basePath)} item={child} isNest={true}></NavbarItem>
                            );
                        })
                    }
                </SubMenu>
            );
        }
    }

    return null;
};

export default NavbarItem;
