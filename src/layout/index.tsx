import React from 'react';

// css
import './index.sass';

import ResetHander from './mixins/ResetHander';

// components
import Navbar from './components/Navbar';
import AppMain from './components/AppMain';

const Layout: React.FC = () => {
    ResetHander();

    return (
        <div className='appWapper'>
            <Navbar></Navbar>

            <div className='main_container'>
                <AppMain></AppMain>
            </div>
        </div>
    );
};

export default Layout;