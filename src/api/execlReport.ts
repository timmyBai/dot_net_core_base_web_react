import request from '@/utils/request';

export const excelReport = () => {
    return request.post('api/execl/report', {}, {
        responseType: 'blob'
    });
};