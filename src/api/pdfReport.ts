import request from '@/utils/request';

export const pdfReport = () => {
    return request.post('api/pdf/report', {}, {
        responseType: 'blob'
    });
};

