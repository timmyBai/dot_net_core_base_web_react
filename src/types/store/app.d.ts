type SidebarType = {
    opened: boolean,
    withoutAnimation: boolean
};

type AppStateType = {
    isLoading: boolean,
    sidebar: SidebarType,
    device: string
};
