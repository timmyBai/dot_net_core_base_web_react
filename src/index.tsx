import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import 'normalize.css';

// css
import '@/styles/index.sass';
import '@/styles/taliwind.sass';

// js
import store from './store';
import reportWebVitals from './reportWebVitals';

// components
import App from './App';
import Router from './router';
import Permission from './Permission';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App>
                <Router>
                    <Permission></Permission>
                </Router>
            </App>
        </Provider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
