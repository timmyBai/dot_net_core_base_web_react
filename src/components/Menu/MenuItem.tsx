import React, { useContext } from 'react';

// css
import './MenuItem.sass';

// context
import MenuContext from './MenuContext';

type MenuItemProps = {
    children: JSX.Element | JSX.Element[]
}

const MenuItem: React.FC<MenuItemProps> = ({ children }) => {
    const context = useContext(MenuContext);

    if (context !== undefined) {
        return (
            <li className={`menuItem ${context.type === 'subMenu'? 'subMenu': ''}`}>
                { children }
            </li>
        );
    }
    
    return (
        <li className='menuItem'>
            { children }
        </li>
    );
};

export default MenuItem;
