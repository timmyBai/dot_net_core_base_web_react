import React from 'react';

// css
import './Menu.sass';

type MenuProps = {
    className: string,
    children: JSX.Element | JSX.Element[]
}

const Menu: React.FC<MenuProps> = ({ className, children }) => {
    return (
        <div className={`menu ${className}`}>
            { children }
        </div>
    );
};

Menu.defaultProps = {
    className: 'menu'
};

export default Menu;
