import { createContext } from 'react';

type MenuContextType = {
    type: string
}

const MenuContext = createContext<MenuContextType>({
    type: ''
});

export default MenuContext;
