import React, { useState } from 'react';

// css
import './SubMenu.sass';

// context
import MenuContext from './MenuContext';

type SubMenuProps = {
    title: string,
    children: JSX.Element | JSX.Element[]
}

const SubMenu: React.FC<SubMenuProps> = ({ title, children }) => {
    const [MenuType] = useState('subMenu');
    const MenuTypeContext = {
        type: MenuType
    };

    return (
        <MenuContext.Provider value={MenuTypeContext}>
            <li className='subMenu'>
                <span className='title'>{title}</span>
                <ol className='nestMenu'>
                    {children}
                </ol>
            </li>
        </MenuContext.Provider>
    );
};

export default SubMenu;
