import React from 'react';

// css
import './index.sass';

// jsx
type HamburgerProps = {
    className: string,
    collapse: () => void
}

const Hamburger: React.FC<HamburgerProps> = ({ className, collapse }) => {
    return (
        <div className={`hamburger ${className}`} onClick={collapse}>
            <div className="hamburger_toggle"></div>
            <div className="hamburger_toggle"></div>
            <div className="hamburger_toggle"></div>
        </div>
    );
};

Hamburger.defaultProps = {
    className: 'hamburger'
};

export default Hamburger;
