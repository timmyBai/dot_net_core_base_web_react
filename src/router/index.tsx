import React from 'react';
import { BrowserRouter, Navigate } from 'react-router-dom';
import RouterWaiter, { OnRouteBeforeType } from './components/RouterWaiter';

// utils
import { getPageTitle } from '@/utils/getPageTitle';

// components
import Layout from '@/layout/';
import Home from '@/views/home/index';
import WebSocketHandle from '@/views/websocket';
import ErrorPage404 from '@/views/errorPage/errorPage404';
import ExcelReport from '@/views/excelReport';
import PdfReport from '@/views/pdfReport';


type RoutersProps = {
    children: JSX.Element | JSX.Element[]
}

// base routes
export const constantRoutes = [
    {
        path: '',
        element: <Layout />,
        children: [
            {
                path: '',
                index: true,
                element: <Home />,
                meta: {
                    title: '首頁'
                }
            }
        ]
    },
    {
        path: '/websocket',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <WebSocketHandle />,
                meta: {
                    title: 'websocket'
                }
            }
        ]
    },
    {
        path: '/excelReport',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <ExcelReport />,
                meta: {
                    title: 'excel 報表下載'
                }
            }
        ]
    },
    {
        path: '/pdfReport',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <PdfReport />,
                meta: {
                    title: 'pdf 報表下載'
                }
            }
        ]
    },
    {
        path: '/404',
        element: <ErrorPage404 />,
        meta: {
            title: '404'
        },
        hidden: true
    },
    {
        path: '*',
        element: <Navigate to='/404' />,
        hidden: true
    }
];

// permission routes
export const asyncRoutes = [
];

// routes component
const Router: React.FC<RoutersProps> = ({ children }) => {
    const onRouteBefore: OnRouteBeforeType = ({ meta }) => {
        document.title = getPageTitle(meta.title);
    };

    return (
        <BrowserRouter>
            <RouterWaiter routes={constantRoutes} onRouteBefore={onRouteBefore}></RouterWaiter>
            {children}
        </BrowserRouter>
    );
};

export default Router;
