/**
 * @param {string} path
 * @returns {Boolean}
 */
export const isExternal = (path: any) => {
    if (path !== undefined) {
        return /^(https?:|mailto:|tel:)/.test(path);
    }

    return false;
};
