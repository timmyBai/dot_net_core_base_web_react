import Cookies from 'js-cookie';

const TokenKey: string | undefined = 'platform-token';

export const getToken = (): string | undefined => {
    return Cookies.get(TokenKey);
};

export const setToken = (token: string, expires: number): string | undefined => {
    return Cookies.set(TokenKey, token, {
        expires: new Date(expires),
        sameSite: 'Strict'
        // 同源政策關係 secure 只有在 https 才有起作用，傳送給伺服器(暫時停用使用此安全)
        // secure: true
    });
};

export const removeToken = (): void => {
    return Cookies.remove(TokenKey);
};