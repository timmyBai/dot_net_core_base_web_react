import { createSlice } from '@reduxjs/toolkit';

// utils
import { getToken } from '@/utils/auth';

const initialState: UserStateType = {
    token: getToken()
};

const reducers = {
    
};

const userReducer = createSlice({
    name: 'user',
    initialState,
    reducers
});

const user = userReducer.reducer;

export default {
    user
};
