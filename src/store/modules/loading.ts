import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: LoadingStateType = {
    isLoading: false
};

const reducers = {
    changeLoading: (state = initialState, action: PayloadAction<boolean>) => {
        state.isLoading = action.payload;
    }
};

const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers
});

const loading = loadingSlice.reducer;

export default {
    loading
};

